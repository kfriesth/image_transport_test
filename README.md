# `image_transport` test

[![build status](https://gitlab.com/InstitutMaupertuis/image_transport_test/badges/kinetic/build.svg)](https://gitlab.com/InstitutMaupertuis/image_transport_test/commits/kinetic)

## Dependencies
```bash
sudo apt install -y ros-kinetic-image-transport ros-kinetic-cv-bridge
```

## Compiling
```bash
cd ~
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone https://gitlab.com/InstitutMaupertuis/image_transport_test.git
cd ..
catkin_make
```

## Launching

```bash
source devel/setup.bash
```

```bash
roslaunch image_transport_test camera.launch
```

Close RViz after a few seconds and inspect the `bag` file.
